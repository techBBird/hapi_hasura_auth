import { Request, Server } from "@hapi/hapi";


export function index(request: Request): string {
    console.log("Processing request", request.info.id);
    return "Hello! Nice to have met you.";
}

