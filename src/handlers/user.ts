import { Request, Server, ResponseToolkit } from "@hapi/hapi";
import { BaseController } from "../http/models/BaseController";

export class CreateUserController extends BaseController {

    protected async executeImpl(req: Request, res: ResponseToolkit): Promise<void | any> {
        try {
            // ... Handle request by creating objects

        } catch (err) {
            return this.fail(res, err)
        }
    }
}