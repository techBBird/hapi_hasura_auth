import { Request, ResponseToolkit } from "@hapi/hapi";

export abstract class BaseController {

    /**
     * This is the implementation that we will leave to the
     * subclasses to figure out. 
     */

    protected abstract executeImpl(req: Request, res: ResponseToolkit): Promise<void | any>;

    /**
     * This is what we will call on the route handler.
     * We also make sure to catch any uncaught errors in the
     * implementation.
     */

    public async execute(req: Request, h: ResponseToolkit): Promise<void> {

        try {
            await this.executeImpl(req, h);
        } catch (err) {
            console.log(`[BaseController]: Uncaught controller error`);
            console.log(err);
            this.fail(h, 'An unexpected error occurred')
        }
    }

    public static jsonResponse(h: ResponseToolkit, code: number, message?: string) {
        const response = h.response(message ?? "Ok")
        response.type("application/json")
        response.code(code)
        return response
    }

    public ok<T>(h: ResponseToolkit, dto?: T) {
        if (!!dto) {
            // res.type('application/json');
            // return res.status(200).json(dto);
            return dto
        } else {
            return BaseController.jsonResponse(h, 200);
        }
    }

    public created(h: ResponseToolkit) {
        return h.response().code(201).type('application/json')
    }

    public clientError(h: ResponseToolkit, message?: string) {
        return BaseController.jsonResponse(h, 400, message ? message : 'Unauthorized');
    }

    public unauthorized(h: ResponseToolkit, message?: string) {
        return BaseController.jsonResponse(h, 401, message ? message : 'Unauthorized');
    }

    public paymentRequired(h: ResponseToolkit, message?: string) {
        return BaseController.jsonResponse(h, 402, message ? message : 'Payment required');
    }

    public forbidden(h: ResponseToolkit, message?: string) {
        return BaseController.jsonResponse(h, 403, message ? message : 'Forbidden');
    }

    public notFound(h: ResponseToolkit, message?: string) {
        return BaseController.jsonResponse(h, 404, message ? message : 'Not found');
    }

    public conflict(h: ResponseToolkit, message?: string) {
        return BaseController.jsonResponse(h, 409, message ? message : 'Conflict');
    }

    public tooMany(h: ResponseToolkit, message?: string) {
        return BaseController.jsonResponse(h, 429, message ? message : 'Too many requests');
    }

    public todo(h: ResponseToolkit) {
        return BaseController.jsonResponse(h, 400, 'TODO');
    }

    public fail(h: ResponseToolkit, error: Error | string | any) {
        console.log(error);
        return h.response({
            message: error.toString()
        }).code(500)
    }
}