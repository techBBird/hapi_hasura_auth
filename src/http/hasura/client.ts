import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from "axios";
import { HASURA_GRAPHQL_ENGINE_BASE_URL, HASURA_ADMIN_SECRET } from "../../config/settings"
// import HasuraHeader from "./types/User"
import { Result } from "../../Results"

import { HasuraClientError, ErrorMessage } from "./Errors/errors";
declare module 'axios' {
    interface AxiosResponse<T = any> extends Promise<T> {
    }
}

abstract class HttpClient {
    protected readonly instance: AxiosInstance;

    constructor(option: AxiosRequestConfig) {

        this.instance = axios.create(option)
        this._initializeResponseInterceptor();
    }

    private _initializeResponseInterceptor = () => {
        this.instance.interceptors.response.use(
            this._handleResponse,
            this._handleError,
        )
    }

    private _handleResponse = ({ data }: AxiosResponse) => {
        let response: any = data
        if (response.errors) {
            return Promise.reject(response.errors)
        }
        return response.data
    }

    private _handleError = (error: any) => Promise.reject(error)

}



export default abstract class HasuraHttpClient extends HttpClient {

    constructor() {
        super({
            baseURL: HASURA_GRAPHQL_ENGINE_BASE_URL,
            headers: {
                "X-Hasura-Admin-Secret": HASURA_ADMIN_SECRET,
            }
        })

    }
    public execute = (query: string, variables: any) => this.instance.post("", {
        "variables": variables,
        "query": query
    });

}

export class ActionClient extends HasuraHttpClient {
    private static classInstance?: ActionClient;

    private constructor() {
        super()
    }

    public static getInstance() {
        if (!this.classInstance) {
            this.classInstance = new ActionClient()
        }
        return this.classInstance
    }

    public async fetchGraphQL(operationsDoc: string, variables?: Record<string, any>
    ) {
        return await this.execute(operationsDoc, variables).then(result => result)

    }

    public async fetchGraphQLData(operationsDoc: string, variables?: Record<string, any>) {
        try {
            const { data } = await this.fetchGraphQL(operationsDoc, variables)
            if (!data) {
                return Result.fail("query error")
            }
            return Result.ok(data)

        } catch (error) {
            if (error instanceof HasuraClientError) {
                return Result.fail<string>(error.message)
            }

        }
    }

}

