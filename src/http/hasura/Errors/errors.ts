

type HasuraErrorResponse = {
    extensions: HasuraExtensionError
    message?: string
}
type HasuraExtensionError = {
    code: string
    path: string
}


export type ErrorMessage = {
    message: string,
    error: string,
    code: string,
    path: string,
    status_code: string
}
export class HasuraClientError extends Error {
    public errorMessage: ErrorMessage
    constructor(message: string) {
        super(message)
        const error: ErrorMessage = JSON.parse(message)
        console.error(error)
        this.errorMessage = error
    }
}